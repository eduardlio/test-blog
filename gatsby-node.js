const path = require('path');
const { createFilePath } = require("gatsby-source-filesystem")
exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  if(node.internal.type === "MarkdownRemark") {
	  const slug = createFilePath({
		  node,
		  getNode,
		  basePath: `posts`
	  })
	  createNodeField({
		  node,
		  name: `slug`,
		  value: `/posts${slug}`
	  })
  }
}

exports.createPages = async ({graphql, actions}) => {
	const { createPage } = actions;
	// get all the posts
	const result = await graphql(`
	query{
		allMarkdownRemark{
			edges{
				node{
					fields {
						slug
					}
				}
			}
		}
	}
	`)
	result.data.allMarkdownRemark.edges.forEach(({node}) => {
		createPage({
			path: node.fields.slug,
			component: path.resolve('./src/templates/blog-post.js'),
			context: {
				slug: node.fields.slug
			}
		})
	})
}