import React from 'react';
import { Link } from 'gatsby';

import Layout from '../components/layout'
export default () => (
	<Layout>
		<h1>Woops you probably meant ___graphql</h1>
		<Link to='/___graphql'>It's like three underscores</Link>
	</Layout>
)