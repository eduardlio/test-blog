import React from 'react'
import { graphql, Link } from 'gatsby'

import Layout from '../components/layout'
import SEO from '../components/seo'

const Blog = ({data}) => {
	const posts = data.allMarkdownRemark.edges;
	return <Layout>
		<SEO title="Blog" />
		<h1>Blog posts</h1>
		{posts.map(({node}) => {
			return <div key={node.id}>
				<h3>{node.frontmatter.title}</h3>
				<p>Published {node.frontmatter.date}</p>
				<p>{node.excerpt}</p>
				<Link to={node.fields.slug}>Read More</Link>
			</div>
		})}
	</Layout>
}
export default Blog;

export const query = graphql`
query {
	allMarkdownRemark {
		edges{
			node{
				id
				frontmatter{
					title
					date
				}
				excerpt
				fields{
					slug
				}
			}
		}
	}
}
`;
