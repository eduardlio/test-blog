import React from 'react';
import { Link } from 'gatsby';
export default props => (
	<li style={{marginRight: '1.3rem', listStyle: 'none'}}>
		<Link to={props.to}>{props.children}</Link>
	</li>
)
