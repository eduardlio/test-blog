import React from "react"
import PropTypes from 'prop-types';
const Lmaos = ({ times = 1 }) => (
  <div className="lmaos">
    {new Array(times).fill("lmao").map((i, index) => (
      <p key={index}>{i}</p>
    ))}
  </div>
)

Lmaos.propTypes = {
	times: PropTypes.number
}
export default Lmaos;