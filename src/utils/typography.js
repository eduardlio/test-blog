import Typography from 'typography'
const typo = new Typography({
	baseFontSize: `16px`,
	bodyFontFamily: ["Source Sans Pro", "sans-serif"],
	headerFontFamily: ["Georgia", "serif"]
})
export const { scale, rhythm, options } = typo;
export default typo;