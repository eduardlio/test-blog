import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
const Post = ({ data }) => {
  const post = data.markdownRemark;
  return (
    <Layout>
      <SEO title="blog-post" />
      <h1>{post.frontmatter.title}</h1>
      <div className="blog-content" dangerouslySetInnerHTML={{__html: post.html}}/>
    </Layout>
  )
}
export default Post;
export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      html
      internal {
        content
      }
      frontmatter{ 
        title
        date
      }
    }
  }
`
